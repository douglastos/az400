# Links AZ-400

### origin do link evernote
https://www.evernote.com/l/ADsE8UuOq-hBg4Dno60nSdL3ca3InZ1p_XY

### Acesso ao azuredevops
https://dev.azure.com

__________________________________________________________________________________________________________
### Professor

email pessoal: renatoalmeidamartins@gmail.com

perfil no Linkedin: https://www.linkedin.com/in/renatodealmeidamartins/

__________________________________________________________________________________________________________

Link para avaliação do curso: https://www.metricsthatmatter.com/url/u.aspx?57144DF40158815031

__________________________________________________________________________________________________________

## Module 1

Devops project generator
https://azuredevopsdemogenerator.azurewebsites.net/ 

Puppet Labs State of Devops Report 2019
https://media.webteam.puppet.com/uploads/2019/11/2019-state-of-devops-report-puppet-circleci-splunk_sml-1-1.pdf 

Latest Puppet Labs State of Devops Report 
https://puppet.com/resources/report/state-of-devops-report/ 

SoapUI Test Task
https://marketplace.visualstudio.com/items?itemName=AjeetChouksey.soapui 

Pester, PowerShell Tester 
https://marketplace.visualstudio.com/items?itemName=richardfennellBM.BM-VSTS-PesterRunner-Task 

JMeter Load Tests
https://docs.microsoft.com/en-us/azure/devops/test/load-test/get-started-jmeter-test?view=azure-devops&viewFallbackFrom=vsts

Jira migration tool
https://marketplace.visualstudio.com/items?itemName=solidify.jira-devops-migration&ssr=false#qna

Trello integration
https://marketplace.visualstudio.com/items?itemName=ms-vsts.services-trello

Module 1 Lab -->  https://www.azuredevopslabs.com/labs/azuredevops/agile/

__________________________________________________________________________________________________________

## Module 2

Git -->  https://git-scm.com/

Posh-git --> install-module posh-git  --> import-module posh-git

Large File Storage -->  https://git-lfs.github.com/

Git-Tfs -->  https://github.com/git-tfs/git-tfs#:~:text=git%2Dtfs%20is%20a%20two,your%20updates%20back%20to%20TFS.

Good git extensions for VSCode
 git lens
 git history
 git blame

Module 2 Lab https://www.azuredevopslabs.com/labs/azuredevops/git/ 

__________________________________________________________________________________________________________

## Module 3

Git flow -->  https://nvie.com/posts/a-successful-git-branching-model/

Github flow -->  https://guides.github.com/introduction/flow/

Azure devops CLI extension --> az extension install --name azure-devops
__________________________________________________________________________________________________________

## Module 4

Nuget client downloads -->  https://www.nuget.org/downloads

Create nuget package -->  https://docs.microsoft.com/en-us/nuget/create-packages/creating-a-package

Lab --> https://www.azuredevopslabs.com/labs/azuredevops/packagemanagement/

__________________________________________________________________________________________________________

## Module 5

Lab 1 --> https://www.azuredevopslabs.com/labs/azuredevops/continuousintegration/ 

Lab 2 --> https://www.azuredevopslabs.com/labs/azuredevops/github-integration/

If the badge doesn't show up, your project (or organization setting) is preventing anonymous access to badges. Turn that feature off at the organization level, and be sure it is also off at the project level. This is a screenshot of the setting:

![image.png](./image.png)
        
       
Lab 3 --> https://www.azuredevopslabs.com/labs/vstsextend/jenkins/

To make it work, you need to disable CSRF protection, by going to "Manage Jenkins" --> "Script console", and adding the code below:
import jenkins.model.Jenkins
def instance = Jenkins.instance
instance.setCrumbIssuer(null)
__________________________________________________________________________________________________________

## Module 6

Sample SQL Injection app --> https://azure.microsoft.com/en-us/resources/templates/101-sql-injection-attack-prevention/

Microsoft Threat Modeling Tool -->  https://docs.microsoft.com/en-us/azure/security/develop/threat-modeling-tool

Lab --> https://www.azuredevopslabs.com/labs/vstsextend/azurekeyvault/

__________________________________________________________________________________________________________

## Module 7

Lab -->  https://www.azuredevopslabs.com/labs/azuredevops/sonarcloud/

__________________________________________________________________________________________________________

## Module 8

VM needs to be V3 to support nested virtualization. Recommended: D4sV3. Resize it before installing Docker Desktop

Install Docker Desktop ( https://hub.docker.com/editions/community/docker-ce-desktop-windows)

Lab -->  https://www.azuredevopslabs.com/labs/vstsextend/aspnetmodernize/

![image_1.png](./image_1.png)

MVC 4 needs to be installed (VS 2019 will ask for that)
Need to go to Package Manager (right click on the project --> Manage nuget packages)
Go to Browse, search for DotnetOpenAut.Core, click update.
__________________________________________________________________________________________________________

## Module 9

Lab --> https://azuredevopslabs.com/labs/vstsextend/whitesource/

__________________________________________________________________________________________________________

## Module 11

Lab 1 --> https://www.azuredevopslabs.com/labs/azuredevops/yaml/

You need to search for the "Web app + SQL" resource, after choosing "Create a Resource"
You can split it in 2, creating it by using the template "web app", then follow steps 1 to 5 and complete the creation of the web app.

Steps 6 to 10 will be done choosing a "new resource", "SQL Databases"

when creating the server, click on "configure database", and instead of "general purpose", choose serverless

choose "connectivity method" as "public endpoint", and set  "Allow Azure services and resources to access this server" as Yes

![image_2.png](./image_2.png)

Lab 2 --> https://www.azuredevopslabs.com/labs/vstsextend/Selenium/ (takes too long to provision 

resources; best if done "offline")

Lab 3 --> https://azuredevopslabs.com/labs/vstsextend/releasegates/

Pay attention that the pre-requisite uses the "release gates" template instead of the usual "Parts unlimited"

Lab 4 -->Almost the same as the Lab from Module 6 (key vault), along with using a variable group in the release pipeline. There's no detailed lab, though. This is doc discussing that:  https://docs.microsoft.com/en-us/azure/devops/pipelines/library/variable-groups?view=azure-devops&tabs=yaml . Student material has it defined

Lab 5 --> Using a dashboard and consuming it using the REST API. Can be followed using the student materials.
__________________________________________________________________________________________________________

## Module 12

Lab --> https://www.azuredevopslabs.com/labs/vstsextend/launchdarkly/ 

__________________________________________________________________________________________________________

## Module 14

Lab -->  https://azuredevopslabs.com/labs/azuredevops/appinsights/

__________________________________________________________________________________________________________

## Module 15

Lab 1 --> https://microsoft.github.io/PartsUnlimited/iac/200.2x-IaC-AZ-400T05AppInfra.html

Lab 2--> https://microsoft.github.io/PartsUnlimited/iac/200.2x-IaCM01AzureAuto.html (might be outdated, as it still references AzureRM instead of Az powershell)

__________________________________________________________________________________________________________

## Module 16

Lab --> https://azuredevopslabs.com/labs/vstsextend/dockerjava/

The template mentioned in the lab isn't to be found in the marketplace. Go to this github project and hit "Deploy to Azure" 
https://github.com/Azure/azure-quickstart-templates/tree/master/101-webapp-managed-mysql

__________________________________________________________________________________________________________

## Module 17

Lab --> https://www.azuredevopslabs.com/labs/vstsextend/kubernetes/

__________________________________________________________________________________________________________

## Module 18

The labs are all based on the PartsUnlimitedMRP project, which is inspired by "They Unicorn Project". It starts here:   https://microsoft.github.io/PartsUnlimitedMRP/

Lab 1 (Chef) -->  https://microsoft.github.io/PartsUnlimitedMRP/iac/200.2x-IaC-ConfigureDevTestLabsusingChef.html

Lab 2 (Puppet) -->  https://microsoft.github.io/PartsUnlimitedMRP/iac/200.2x-IaC-DeployappwithPuppetonAzure.html

Lab 3 (Ansible) -->  https://microsoft.github.io/PartsUnlimitedMRP/iac/200.2x-IaC-AnsiblewithAzure.html

Lab 4 (Terraform) -->  https://azuredevopslabs.com/labs/vstsextend/terraform/

__________________________________________________________________________________________________________

## Module 19

SecOps Kit --> https://azsk.azurewebsites.net/01-Subscription-Security/Readme.html

__________________________________________________________________________________________________________

## Module 20

Lab -->  https://azuredevopslabs.com/labs/vstsextend/teams/

Office 365 E3 trial --> https://signup.microsoft.com/create-account/signup?OfferId=B07A1127-DE83-4a6d-9F85-2C104BDAE8B4&dl=ENTERPRISEPACK&ali=1&products=cfq7ttc0k59j:0009

__________________________________________________________________________________________________________

## Links úteis:

Integração do Teams com Moodle:  https://docs.microsoft.com/pt-br/microsoftteams/install-moodle-integration

Centenas de bons labs para experimentar com Azure Devops:  https://azuredevopslabs.com/
